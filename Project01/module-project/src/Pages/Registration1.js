import React, { useState  } from 'react'
import axios from 'axios';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  Col,
  ModalFooter,
} from "reactstrap";
import { useNavigate } from 'react-router';
function Registration1(args) {
  const navigate = useNavigate();
    const [modal, setModal] = useState(false);
    const [formdata, setFormdata] = useState({
        username: "",
        email: "",
        password: "",
        mobileNumber: "",
    });

    const handleInput = (e) => {
        const {name, value} = e.target;
        setFormdata({
            ...formdata,
            [name]: value,
        });
    };

    console.log(formdata);

    const toggle = (e) => {
      if (e) {
        e.preventDefault();
      }
        
        setModal(!modal);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            let response = await axios.post (
                "http://127.0.0.1:3001/registration", formdata
            );
            console.log(response);
            alert("Registered Successfully");
            navigate('/');
        }
        catch(err){
            throw err;
        }
    }; 
  return (
    <div>
      
      <Modal isOpen = {modal} toggle = {toggle} {...args}>
        <ModalHeader toggle = {toggle}>Modal title</ModalHeader>
        <ModalBody>
          <Form onSubmit = {handleSubmit}>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <label for = "username">Username</label>
                  <Input
                    id="username"
                    name="username"
                    placeholder="username"
                    type="text"
                    value={formdata.username}
                    onChange={handleInput}
                    required
                    />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="examplePassword">Password</Label>
                  <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="password"
                  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                  value={formdata.password}
                  onChange={handleInput}
                  required/>
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
              id="email"
              name="email"
              placeholder="Email"
              type="email"
              value={formdata.email}
              onChange={handleInput}
              required
              />
            </FormGroup>
            <FormGroup>
              <Label for="mobileNumber">mobileNumber</Label>
              <Input
              id="mobileNumber"
              name="mobileNumber"
              placeholder="Mobile Number"
              type="tel"
              value={formdata.mobileNumber}
              onChange={handleInput}
              required
              />
            </FormGroup>
          
            <a href="Register for new user!"></a>
            <Button type="submit">Register</Button>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default Registration1
