import axios from 'axios'
import React, { useRef } from 'react'

function Insert() {
    const _idRef = useRef(null)
    const nameRef = useRef(null)
    const ageRef = useRef(null)
    const salaryRef = useRef(null)
    const mailRef = useRef(null)
    const passwordRef = useRef(null)
    const locationRef = useRef(null)
    const handleInput = ()=>{
        const newStd = {
            "_id": parseInt(_idRef.current.value),
            "name": nameRef.current.value,
            "age": ageRef.current.value,
            "salary": parseInt(salaryRef.current.value),
            "mail": mailRef.current.value,
            "password": passwordRef.current.value,
            "location": locationRef.current.value,
        }

        axios.post("http://127.0.0.1:3001/register",newStd).then((res)=>{console.log(newStd);})
        
    }
    return (
    <div>
      <fieldset>Register Studentdata
        <input type='text' ref={_idRef} placeholder=' _id' />
        <input type='text' ref={nameRef} placeholder='name' />
        <input type='text' ref={ageRef} placeholder='age' />
        <input type='text' ref={salaryRef} placeholder='salary' />
        <input type='text' ref={mailRef} placeholder='mail' />
        <input type='text' ref={passwordRef} placeholder='password' />
        <input type='text' ref={locationRef} placeholder='location' />
        <button onClick={handleInput}>Insert</button>
      </fieldset>
    </div>
  )
}

export default Insert