import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input } from "reactstrap";
import { Link } from "react-router-dom";
import axios from "axios";
import Registration1 from "./Registration1";

const Login1 = ({ isOpen, toggle, onLoginSuccess }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginError, setLoginError] = useState(null);
  const [isRegisterModalOpen, setRegisterModalOpen] = useState(false);

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.get(
        `http://localhost:3001/login1/${email}/${password }`,
        
      );

      if (response.data) {
        console.log("Login successful");
        alert("logged in")
        toggle();
      } else {
        console.log("Login failed");
        setLoginError("Your credentials are incorrect");
      }
    } catch (err) {
      console.error("Login error:", err.response?.data?.error || "An error occurred during login");
      setLoginError(err.response?.data?.error || "Fill the required feilds");
    }
  };

  const toggleRegisterModal = () => {
    setRegisterModalOpen(!isRegisterModalOpen);
  };
  return (
    <div>
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Login</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleLogin}>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="text"
                name="email"
                id="email"
                placeholder="Enter your email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password" style={{backgroundColor : "lightblue"
            }}>Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="Enter your password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
            {loginError && <p style={{ color: "red" }}>{loginError}</p>}
            <Button color="primary" type="submit">
              Login
            </Button>{" "}
            <Link to="#" onClick={toggleRegisterModal}>
                Register here.
            </Link>
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </Form>
        </ModalBody>
        </Modal>

        <Registration1 isOpen={isRegisterModalOpen} toggle={toggleRegisterModal} />
    </div>
  );
};

export default Login1;