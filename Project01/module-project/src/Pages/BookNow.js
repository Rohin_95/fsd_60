import axios from 'axios'
import React, { useRef } from 'react'

function BookNow(props) {
    const nameRef =useRef(null);
    const emailRef =useRef(null);
    const mobileRef =useRef(null);
    const handleConfirm = () =>{
        let Booking_Data ={
            "name":nameRef.current.value,
            "email":emailRef.current.value,
            "mobile":mobileRef.current.value
        }
        axios.post("http://localhost:3001/insertbooking", Booking_Data)
        .then((res)=> console.log(res.data))
        .catch((err)=> console.log(err))
    }
  return (
    <div>
        <div className='title'><h1>Enter Your Details</h1></div>
        <h5>We will use these details to share your booking information</h5>
      <fieldset>
        <input type='text' ref={nameRef} placeholder='Enter Full Name' style={{backgroundColor : ""}} />
        <input type='email' ref={emailRef} placeholder='Enter Email Address' style={{backgroundColor : ""}}  />
        <input type='tel' ref={mobileRef} placeholder='Enter Mobile Number'style={{backgroundColor : ""}}  />
        <button onClick={handleConfirm} style={{backgroundColor : "green"}} >Confirm</button>
      </fieldset>
    </div>
  )
}

export default BookNow
