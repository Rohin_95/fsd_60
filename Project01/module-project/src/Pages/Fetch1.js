import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

function Fetch1() {
  const [roomDetails, setRoomDetails] = useState({});
  const { name} = useParams();

  useEffect(() => {
    // Fetch product details based on the ID
    axios
      .get(`http://localhost:3001/fetch1/${name}`)
      .then((res) => {
        setRoomDetails(res.data);
      })
      .catch((err) => console.log(err));
  }, [name]);

  return (
    <div>
      {/* Display product details */}
      <h2>{roomDetails.name}</h2>
      <p>{roomDetails.location}</p>
      {/* Other details... */}
    </div>
  );
}

export default Fetch1;

