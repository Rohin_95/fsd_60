import axios from 'axios'
import React, { useRef } from 'react';
// import {roomPriceRef, roomNameRef, roomurlref, roomLocationRef} from 'reactstrap'

function Insert() {
    const roomurlref =useRef(null)
    const roomNameRef = useRef(null)
    
    const roomPriceRef = useRef(null)
    const roomDescriptionRef = useRef(null)
    
    const roomLocationRef =useRef(null)

    const handleInput = ()=>{
        const newRoom = {
          "url":roomurlref.current.value,
            "name":  roomNameRef.current.value,
            
            "price": roomPriceRef.current.value,
            "description": roomDescriptionRef.current.value,
            
            "location":roomLocationRef.current.value,
        }

      axios.post("http://127.0.0.1:3001/newdb",newRoom).then((res)=>{console.log(newRoom);})
        
    }
    return (
    <div>
      <fieldset>Hotel Details
      <input type='text' ref={roomurlref} placeholder='Image Url' /><br></br><br></br>
        <input type='text' ref={roomNameRef} placeholder='Room Name' /><br></br><br></br>
        
        <input type='text' ref={roomPriceRef} placeholder='Room price' /><br></br><br></br>
        <input type='text' ref={roomDescriptionRef} placeholder='Room Description' /><br></br><br></br>
        
        <input type='text' ref={roomLocationRef} placeholder='Room Location ' /><br></br><br></br>

        <button type='submit' onClick={handleInput}>Insert</button>
      </fieldset>
    </div>
  )
}

export default Insert