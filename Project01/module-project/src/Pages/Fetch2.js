import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

function Fetch2() {
    const [roomDetails, setRoomDetails] = useState({});
    const { name} = useParams();
  
    useEffect(() => {
      // Fetch product details based on the ID
      axios
        .get(`http://localhost:3001/fetchrooms`)
        .then((res) => {
          setRoomDetails(res.data);
        })
        .catch((err) => console.log(err));
    }, [name]);
  
    return (
      <div>
        {/* Display product details */}
        <h1>{roomDetails.name}</h1>
        <p>{roomDetails.location}</p>
        <p>{roomDetails.description}</p>
        {/* Other details... */}
      </div>
    );
}

export default Fetch2
