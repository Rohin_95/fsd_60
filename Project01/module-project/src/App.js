import { BrowserRouter, Routes, Route, } from 'react-router-dom';
import NavBar from "./Components/NavBar";
import Places from "./Pages/Places";
import Header from './Components/Header';
import Footer from "./Components/Footer";
import Login1 from "./Pages/Login1";
import Images from './Components/Images';

import './App.css';
import Carts from "./Pages/Carts"; 
import Fetch2 from './Pages/Fetch2';
 import Hyderabad from './Components/Hyderabad';
import NavBar1 from './Components/NavBar1';
import Home from './Components/Home';
import Banglore from './Components/Banglore'
import Chennai from './Components/Chennai'
import Delhi from './Components/Delhi'
import Mumbai from './Components/Mumbai'
import BookNow from './Pages/BookNow';
import About from './Components/About';
import Contact from './Components/Contact';




function App() {
  return (
    <div>
      <BrowserRouter>
       <Header />
          <NavBar />
          <NavBar1 />
        <Routes>
         
          
          {/* <Route path = '/' element= {[<Header /> , <NavBar />, <Images />, <Footer />]}/>
          <Route path = '/login' component= {<Login1 />}/>
          <Route path = '/places' compomnet= {<Places />}/>
          <Route path='/hyderabad' element={[<Header />, <Hyderabad />]} /> */}
           {/* <Route path = '/Hyderabad' compomnet= {<Hyderabad />}/> */}

           <Route path ='/' element = {<Home/>}/> 
         <Route path ='/Hyderabad' element = {<Hyderabad/>}/> 
         <Route path ='/about' element = {<About/>}/> 
         <Route path ='/contact' element = {<Contact/>}/> 
       
        <Route path ='/Chennai' element = {<Chennai />}/> 
        <Route path ='/Delhi' element = {<Delhi />}/> 
        <Route path ='/Mumbai' element = {<Mumbai />}/> 
        <Route path ='/Banglore' element = {<Banglore />}/>
        <Route path ='/booking' element = {<BookNow />}/>
        
        </Routes> 
      </BrowserRouter>
      <Fetch2 />
      <Footer/>
    </div>
  );
}

export default App;
