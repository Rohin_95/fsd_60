import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Card, CardBody, CardSubtitle, CardTitle, Col } from 'reactstrap';
import BookNow from '../Pages/BookNow';
import { Link } from 'react-router-dom';


function Hyderabad() {
    const [records, setRecords] = useState([]);
    useEffect(()=>{
        axios.get("http://localhost:3001/fetchrooms")
        .then((res)=> setRecords(res.data))
        .catch((err)=>{console.log(err);})
    },[])
  return (
    <div>
        {/* <h1>Helloyjfcgvbhj</h1> */}

      {records.map(record =>(
           
                <Card bodycolor="primary" style={{ width:'100%', display:'flex', flexDirection:'row', marginBottom:'10px'}}>
                    <div style={{width: '500px',padding:'5px'}}><img alt="Sample" src={record.url} /></div>
                    <div style={{marginLeft:'160px'}}><h2 >{record.name}</h2>
                        <CardBody>
                            <CardSubtitle className="mb-2 text" tag="h6">
                            {record.description}
                            </CardSubtitle>
                            <CardTitle tag="h5"><p>Rs. {record.price}</p></CardTitle>
                            
                            <Button>
                                <Link to ='/booking'>BOOK NOW</Link>
                            </Button>
                        </CardBody>
                    </div>
                </Card>
            
        ))}
    </div>
  )
}

export default Hyderabad
