import React from 'react'
import  {useState} from 'react';
import {NavItem,
  
   Navbar,
   NavbarBrand,
   NavLink,
   DropdownItem,
   DropdownToggle,
   UncontrolledDropdown,
   DropdownMenu,
   Input,
   InputGroup,
   InputGroupAddon,
   Button,
  } from 'reactstrap';
import { Link, } from 'react-router-dom'
import Login1 from '../Pages/Login1';





const NavBar = () => {
  const [isLoginModalOpen, setLoginModalOpen] = useState(false);
  const [userDetails] = useState(null); 

  const toggleLoginModal = () => {
    setLoginModalOpen(!isLoginModalOpen);
  };


  return (
    <div>
      <Navbar  style={{backgroundColor: 'red'}}>
      
            {/* <UncontrolledDropdown>
              <DropdownToggle nav caret>
                Places
              </DropdownToggle>
              <DropdownMenu left>
                <DropdownItem><Link to='/hyderabad'>Hyderabad</Link></DropdownItem>
                <DropdownItem>Banglore</DropdownItem>
                <DropdownItem>Chennai</DropdownItem>
                <DropdownItem>Mumbai</DropdownItem>
                <DropdownItem>Delhi</DropdownItem>
                <DropdownItem>Noida</DropdownItem>
                <DropdownItem>Pune</DropdownItem>
                <DropdownItem divider />
                <DropdownItem>Reset</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown> */}
            {/* <Login1 /> */}
            <Link className='p-2' to= '/' style={{ color: 'white', textDecoration: 'none'}}>Home</Link>
            <Link className='p-2' to= '/About' style={{ color: 'white', textDecoration: 'none'}}>About</Link>
            <Link className='p-2' to= '/Contact' style={{ color: 'white', textDecoration: 'none'}}>Contact</Link>
            
            <Link to="#" onClick={toggleLoginModal}>
              <button>
                Login 
              </button>
              <Login1 isOpen={isLoginModalOpen} toggle={toggleLoginModal} />
            </Link>
            

  
      </Navbar>

    </div>
  
  )

  }

export default NavBar


